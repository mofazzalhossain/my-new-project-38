<h1 style="background:#17806b;#FFF;padding:15px ">Math Operator</h1>
<?php

$a = 02;
$b = 5;
$c = $a+$b;
echo $c;

echo "<br/>";
$a = 02;
$b = 5;
$c = $a-$b;
echo $c;

echo "<br/>";
$a = 02;
$b = 5;
$c = $a*$b;
echo $c;

echo "<br/>";
$a = 02;
$b = 5;
$c = $a/$b;
echo $c;

echo "<br/>";
$a = 02;
$b = 5;
$c = $a%$b;
echo $c;


echo "<br/>";
$a = 02;
$b = 5;
$c = $a**$b;
echo $c;

echo "<br/>";
$x = 10;
echo $x;

echo "<br/>";
$x = 20;
$x += 100;

echo $x;


echo "<br/>";
$x = 50;
$x -= 30;

echo $x;

echo "<br/>";
$x = 10;
$y = 6;

echo $x * $y;

echo "<br/>";
$x = 10;
$x /= 5;

echo $x;

echo "<br/>";
$x = 15;
$x %= 4;

echo $x;

echo "<br/>";
$x = 100;
$y = "100";

var_dump($x == $y); // returns true because values are equal

echo "<br/>";
$x = 100;
$y = "100";

var_dump($x === $y); // returns false because types are not equal

echo "<br/>";
$x = 100;
$y = "100";

var_dump($x != $y); // returns false because values are equal