<?php
echo 'Hello World';
?>

<br/>
echo '0:        '.(boolval(0) ? 'true' : 'false')."\n";

<br/>
$var = 0;

// Evaluates to true because $var is empty
if (empty($var)) {
echo '$var is either 0, empty, or not set at all';
}

<br/>
$yes = array('this', 'is', 'an array');

echo is_array($yes) ? 'Array' : 'not an Array';
echo "\n";

$no = 'this is a string';

echo is_array($no) ? 'Array' : 'not an Array';

<br/>

define("GREETING", "WELCOME TO BITM!");
echo greeting;
